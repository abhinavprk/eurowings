#!/bin/sh
# Script to run docker image 
echo Build docker image eurowings-docker 

docker build -f Dockerfile -t eurowings-docker .

echo Docker image eurowings-docker built 