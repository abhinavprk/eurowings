FROM openjdk:8
ADD target/eurowings.jar eurowings.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","eurowings.jar"]