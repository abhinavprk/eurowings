package com.eurowings.task.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.eurowings.task.model.Subscriber;
import com.eurowings.task.service.SubscriberService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SubscriberControllerTest {
	

	
	@Mock
	private SubscriberController subCont;

	@MockBean
	private SubscriberService service;
	
	@Autowired
	private TestRestTemplate template;
	
	List<Subscriber> subList = new ArrayList<Subscriber>();
	Subscriber sus = new Subscriber(1l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13");
	
	
	@Before
	public void setup() throws Exception {
		subList.add(sus);	
 }
	
	
	@Test
	public void getAllUsersFromDatabaseTest() throws Exception
	{
		Mockito.when(service.getAll()).thenReturn(subList);
		ResponseEntity<List> response = template.getForEntity("/api/all", List.class);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Map<String, String> sub = (Map<String, String>) response.getBody().get(0);
		String[] result = {String.valueOf(sub.get("id")),sub.get("userName").toString(),sub.get("firstName").toString(),sub.get("lastName").toString()
				,sub.get("email").toString(),sub.get("startDate").toString(),sub.get("endDate").toString()};
		Assert.assertArrayEquals(expected, result);   
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	@Test
	public void getAllSubscribersTest() throws Exception
	{
		Mockito.when(service.getAllSubs()).thenReturn(subList);
		ResponseEntity<List> response = template.getForEntity("/api/allsubs", List.class);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Map<String, String> sub = (Map<String, String>) response.getBody().get(0);
		String[] result = {String.valueOf(sub.get("id")),sub.get("userName").toString(),sub.get("firstName").toString(),sub.get("lastName").toString()
				,sub.get("email").toString(),sub.get("startDate").toString(),sub.get("endDate").toString()};
		Assert.assertArrayEquals(expected, result);   
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	@Test
	public void getAllUnSubscribersTest() throws Exception
	{
		Mockito.when(service.getAllUnSubs()).thenReturn(subList);
		ResponseEntity<List> response = template.getForEntity("/api/allunsubs", List.class);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Map<String, String> sub = (Map<String, String>) response.getBody().get(0);
		String[] result = {String.valueOf(sub.get("id")),sub.get("userName").toString(),sub.get("firstName").toString(),sub.get("lastName").toString()
				,sub.get("email").toString(),sub.get("startDate").toString(),sub.get("endDate").toString()};
		Assert.assertArrayEquals(expected, result);   
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	@Test
	public void getSubscriberByUserNameTestPositive() throws Exception
	{
		Mockito.when(service.findByUserName(Mockito.anyString())).thenReturn(sus);
		ResponseEntity<Subscriber> response = template.getForEntity("/api/subs/uname", Subscriber.class);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Subscriber sub = response.getBody();
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName(),sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		Assert.assertArrayEquals(expected, result);   
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	
	//Test for Exception
	//@Test(expected = SubscriptionException.class)
	@Test
	public void getSubscriberByUserNameTestNegative() throws Exception
	{
		Mockito.when(service.findByUserName(Mockito.anyString())).thenReturn(null);
		ResponseEntity<Subscriber> response  = template.getForEntity("/api/subs/uname", Subscriber.class);
		Assert.assertEquals(500,response.getStatusCode().value());		
	}
	
	
	@Test
	public void addSubscriberTest() throws Exception
	{
		Mockito.when(service.save(Mockito.any(Subscriber.class))).thenReturn(sus);
		HttpEntity<Object> subscriber = getHttpEntity("{\r\n" + 
				"        \"userName\": \"anim\",\r\n" + 
				"        \"firstName\": \"Kiki\",\r\n" + 
				"        \"lastName\": \"John\",\r\n" + 
				"        \"email\": \"email@kiki.com\",\r\n" + 
				"        \"startDate\": \"2018-12-13\",\r\n" + 
				"        \"endDate\": \"2019-05-13\"\r\n" + 
				"    }");
		ResponseEntity<Subscriber> response = template.postForEntity("/api/newsubs",subscriber,Subscriber.class);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Subscriber sub = response.getBody();
		/*String[] result = {String.valueOf(sub.get("id")),sub.get("userName").toString(),sub.get("firstName").toString(),sub.get("lastName").toString()
				,sub.get("email").toString(),sub.get("startDate").toString(),sub.get("endDate").toString()};*/
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName(),sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		Assert.assertArrayEquals(expected, result);   
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	
	@Test
	public void getSubscribedAfterOrBeforeTestPositive() throws Exception
	{
		Mockito.when(service.getSubscribedAfterOrBefore(Mockito.anyString(),Mockito.anyBoolean())).thenReturn(subList);
		ResponseEntity<List> response = template.getForEntity("/api/subscrba/2018-01-12/true", List.class);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Map<String, String> sub = (Map<String, String>) response.getBody().get(0);
		String[] result = {String.valueOf(sub.get("id")),sub.get("userName").toString(),sub.get("firstName").toString(),sub.get("lastName").toString()
				,sub.get("email").toString(),sub.get("startDate").toString(),sub.get("endDate").toString()};
		Assert.assertArrayEquals(expected, result);   
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	
	
	@Test
	public void isSubscribedTest() throws Exception
	{
		Mockito.when(service.isSubscribed(Mockito.anyString())).thenReturn(true);
		ResponseEntity<Boolean> response = template.getForEntity("/api/issubscr/uname", Boolean.class);
		Boolean result = response.getBody();
		Assert.assertTrue(result);
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	@Test
	public void deleteSubscriptionTest() throws Exception
	{
		Mockito.when(service.deleteSubscription(Mockito.anyString())).thenReturn(true);
		ResponseEntity<Boolean> response = template.getForEntity("/api/unsubscr/uname", Boolean.class);
		Boolean result = response.getBody();
		System.out.println("Abhianv value" +result );
		Assert.assertTrue(result);
		Assert.assertEquals(200,response.getStatusCode().value());
		
	}
	
	private HttpEntity<Object> getHttpEntity(Object body) {
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    return new HttpEntity<Object>(body, headers);
	  }

}
