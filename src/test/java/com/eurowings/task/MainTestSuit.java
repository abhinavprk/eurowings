package com.eurowings.task;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.eurowings.task.controller.SubscriberControllerTest;
import com.eurowings.task.service.SubscriberServiceImplTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	EurowingsApplicationTests.class,
	SubscriberControllerTest.class,
	SubscriberServiceImplTest.class
	})


public class MainTestSuit {

}
