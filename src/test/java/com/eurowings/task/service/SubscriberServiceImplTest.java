package com.eurowings.task.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import com.eurowings.task.exception.SubscriptionException;
import com.eurowings.task.model.Subscriber;
import com.eurowings.task.repository.SubscriberRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SubscriberServiceImplTest {
	
	@MockBean
	private SubscriberRepository repo;
	
	@Autowired
	private SubscriberService service;
	
	List<Subscriber> subList = new ArrayList<Subscriber>();
	Subscriber sus = new Subscriber(1l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13");
	
	
	@Before
	public void setup() throws Exception {
		subList.add(sus);
		}

	
	@Test
	public void getAllTest() throws Exception{
		Mockito.when(repo.findAll()).thenReturn((Iterable<Subscriber>) subList);
		List<Subscriber> mkList = service.getAll();
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Subscriber sub = mkList.get(0);
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName()
				,sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		int[] expectedSize = {1};
		int[] resultSize = {mkList.size()};
		Assert.assertArrayEquals(expectedSize,resultSize);
		Assert.assertArrayEquals(expected, result);		
	}
	
	
	@Test(expected = SubscriptionException.class)
	public void saveEndDateBeforeStartDateTest() throws Exception{
		Subscriber subsc = new Subscriber(1l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2025-09-16", "2020-12-13");			
		service.save(subsc);		
		
	}
	
	@Test(expected = SubscriptionException.class)
	public void saveEndDateBeforeTodayTest() throws Exception{
		Subscriber subsc = new Subscriber(1l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2020-09-16", "2017-09-16");			
		service.save(subsc);		
		
	}
	
	@Test(expected = SubscriptionException.class)
	public void saveStartDateBeforeTodayTest() throws Exception{
		Subscriber subsc = new Subscriber(1l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2015-09-16", "2020-09-16");			
		service.save(subsc);		
		
	}
	
	
	@Test
	public void saveWhenUserAlreadyInDBPositiveTest() throws Exception{		
		Subscriber susin = new Subscriber(0l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2020-09-16", "2021-12-13");
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));
		Mockito.when(repo.updateSubscription(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn((1));
		Subscriber resSub = service.save(susin);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2020-09-16", "2021-12-13"};
		String[] result = {String.valueOf(resSub.getId()),resSub.getUserName(),resSub.getFirstName(),resSub.getLastName()
				,resSub.getEmail(),resSub.getStartDate(),resSub.getEndDate()};
		Assert.assertArrayEquals(expected, result);		
		
	}
	
	@Test
	public void saveWhenUserAlreadyInDBNegativeTest() throws Exception{		
		Subscriber susin = new Subscriber(0l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2020-09-16", "2021-12-13");
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));
		Mockito.when(repo.updateSubscription(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn((0));
		Subscriber resSub = service.save(susin);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		String[] result = {String.valueOf(resSub.getId()),resSub.getUserName(),resSub.getFirstName(),resSub.getLastName()
				,resSub.getEmail(),resSub.getStartDate(),resSub.getEndDate()};
		Assert.assertArrayEquals(expected, result);		
		
	}
	
	
	@Test
	public void saveWhenUserIsNotAlreadyInDBTest() throws Exception{		
		Subscriber susin = new Subscriber(0l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2020-09-16", "2021-12-13");
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((null));
		Mockito.when(repo.save(Mockito.any())).thenReturn((susin));
		Subscriber resSub = service.save(susin);
		String[] expected = {"axmya", "Amaya", "Nayana", "abc@gmail.com", "2020-09-16", "2021-12-13"};
		String[] result = {resSub.getUserName(),resSub.getFirstName(),resSub.getLastName()
				,resSub.getEmail(),resSub.getStartDate(),resSub.getEndDate()};
		Assert.assertArrayEquals(expected, result);		
		
	}
	
	@Test(expected = SubscriptionException.class)
	public void saveWhenUserIsNotAlreadyInDBExceptionTest() throws Exception{		
		Subscriber susin = new Subscriber(0l, "axmya", "Amaya", "Nayana", "abc@gmail.com", "2020-09-16", "2021-12-13");
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((null));
		Mockito.when(repo.save(Mockito.any())).thenThrow(new DataIntegrityViolationException("Email"));
		System.out.println("The Exception has thrown");
		service.save(susin);	
		System.out.println("The Exception has not thrown");
	}
	
	
	@Test
	public void findByUserNameWhenUserExistsTest() throws Exception{
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));
		Subscriber sub = service.findByUserName("axmya");
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName()
				,sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		Assert.assertArrayEquals(expected, result);		
	}
	
	@Test (expected = SubscriptionException.class)
	public void findByUserNameWhenUserDoesNotExistsTest() throws Exception{
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((null));
		service.findByUserName("axmya");
		
	}
	
	@Test
	public void isSubscribedWhenUserIsInDBTest() throws Exception{
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertTrue(result);
	}
	
	@Test
	public void isSubscribedWhenUserIsInDBAndStDateEndDateEqualCurrentDateTest() throws Exception{
		sus.setStartDate(LocalDate.now().toString());
		sus.setEndDate(LocalDate.now().toString());
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertTrue(result);
	}
	
	@Test
	public void isSubscribedWhenUserIsInDBAndStDateBeforeEndDateEqualCurrentDateTest() throws Exception{
		sus.setStartDate(LocalDate.now().minus(Period.ofDays(15)).toString());
		sus.setEndDate(LocalDate.now().toString());
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertTrue(result);
	}
	
	
	@Test
	public void isSubscribedWhenUserIsInDBAndStDateBeforeEndDateAfterCurrentDateTest() throws Exception{
		sus.setStartDate(LocalDate.now().minus(Period.ofDays(15)).toString());
		sus.setEndDate(LocalDate.now().plus(Period.ofDays(15)).toString());
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertTrue(result);
	}
	
	@Test
	public void isSubscribedWhenUserIsInDBAndStDateEqualEndDateAfterCurrentDateTest() throws Exception{
		sus.setStartDate(LocalDate.now().toString());
		sus.setEndDate(LocalDate.now().plus(Period.ofDays(15)).toString());
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertTrue(result);
	}
	
	@Test
	public void isSubscribedWhenUserIsInDBAndStDateAfterEndDateAfterCurrentDateTest() throws Exception{
		sus.setStartDate(LocalDate.now().plus(Period.ofDays(7)).toString());
		sus.setEndDate(LocalDate.now().plus(Period.ofDays(15)).toString());
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertFalse(result);
	}
	
	
	@Test
	public void isSubscribedWhenUserIsInDBAndStDateEqualEndDateBeforeCurrentDateTest() throws Exception{
		sus.setStartDate(LocalDate.now().toString());
		sus.setEndDate(LocalDate.now().minus(Period.ofDays(15)).toString());
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn((sus));		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertFalse(result);
	}
	
	
	@Test
	public void isSubscribedWhenUserIsNotInDBTest() throws Exception{
		Mockito.when(repo.findByUserName(Mockito.anyString())).thenReturn(null);		
		Boolean result = service.isSubscribed("axmya");		
		Assert.assertFalse(result);
	}
	
	
	@Test
	public void getAllSubsTest() throws Exception{
		Mockito.when(repo.findByStartDateBeforeAndEndDateAfter(Mockito.anyString(),Mockito.anyString())).thenReturn(subList);
		List<Subscriber> mkList = service.getAllSubs();
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Subscriber sub = mkList.get(0);
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName()
				,sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		int[] expectedSize = {1};
		int[] resultSize = {mkList.size()};
		Assert.assertArrayEquals(expectedSize,resultSize);
		Assert.assertArrayEquals(expected, result);		
	}
	
	@Test
	public void getAllUnSubsTest() throws Exception{
		Mockito.when(repo.findByStartDateAfterOrEndDateBefore(Mockito.anyString(),Mockito.anyString())).thenReturn(subList);
		List<Subscriber> mkList = service.getAllUnSubs();
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		Subscriber sub = mkList.get(0);
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName()
				,sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		int[] expectedSize = {1};
		int[] resultSize = {mkList.size()};
		Assert.assertArrayEquals(expectedSize,resultSize);
		Assert.assertArrayEquals(expected, result);		
	}
	
	@Test
	public void deleteSubscriptionTest() throws Exception{
		Mockito.doNothing().when(repo).deleteByUserName(Mockito.anyString());
		Boolean result = service.deleteSubscription("axmya");		
		Assert.assertTrue(result);
	}
	
	
	@Test
	public void deleteSubscriptionExceptionTest() throws Exception{
		Mockito.doThrow(new IllegalArgumentException()).when(repo).deleteByUserName(Mockito.anyString());
		Boolean result = service.deleteSubscription("axmya");		
		Assert.assertFalse(result);
	}
	
	@Test
	public void getSubscribedAfterOrBeforeAfterTest() throws Exception{
		Mockito.when(repo.findByStartDateAfter(Mockito.anyString())).thenReturn((subList));
		List<Subscriber> mkList = service.getSubscribedAfterOrBefore(Mockito.anyString(),true);
		Subscriber sub = mkList.get(0);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName()
				,sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		Assert.assertArrayEquals(expected, result);		
	}
	
	@Test
	public void getSubscribedAfterOrBeforeBeforeTest() throws Exception{
		Mockito.when(repo.findByStartDateBefore(Mockito.anyString())).thenReturn((subList));
		List<Subscriber> mkList = service.getSubscribedAfterOrBefore(Mockito.anyString(),false);
		Subscriber sub = mkList.get(0);
		String[] expected = {"1", "axmya", "Amaya", "Nayana", "abc@gmail.com", "2018-09-16", "2020-12-13"};
		String[] result = {String.valueOf(sub.getId()),sub.getUserName(),sub.getFirstName(),sub.getLastName()
				,sub.getEmail(),sub.getStartDate(),sub.getEndDate()};
		Assert.assertArrayEquals(expected, result);		
	}
	
		
}
