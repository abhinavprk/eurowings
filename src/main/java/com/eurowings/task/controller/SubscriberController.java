package com.eurowings.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eurowings.task.exception.SubscriptionException;
import com.eurowings.task.model.Subscriber;
import com.eurowings.task.service.SubscriberService;

/**
 * 
 * @author abhinav
 */

@RestController
@RequestMapping("/api")
public class SubscriberController {
  
	@Autowired
	SubscriberService service;
	
	/**
	 * 
	 * @return All entries from the subscriber table.
	 */
	@GetMapping(path = "/all")
	public ResponseEntity<List<Subscriber>> getAllUsersFromDatabase() {
	    return ResponseEntity.ok(service.getAll());
	  }
	
	/**
	 * 
	 * @return All entries with active subscription.
	 */
	@GetMapping(path = "/allsubs")
	public ResponseEntity<List<Subscriber>> getAllSubscribers() {
	    return ResponseEntity.ok(service.getAllSubs());
	  }
	
	/**
	 * 
	 * @return All entries with inactive subscription.
	 */
	@GetMapping(path = "/allunsubs")
	public ResponseEntity<List<Subscriber>> getAllUnSubscribers() {
	    return ResponseEntity.ok(service.getAllUnSubs());
	  }
  
	/**
	 * 
	 * @param uname the user name of the subscriber
	 * @return the Subscriber object for the given user
	 * @throws SubscriptionException 
	 */
	@GetMapping(path = "/subs/{uname}")
	public ResponseEntity<Subscriber> getSubscriberByUserName(@PathVariable String uname) throws SubscriptionException
	{
		Subscriber subs = new Subscriber();
		Subscriber subDB = service.findByUserName(uname);
		if(subDB != null)
		{
		subs = subDB;
		}
		else
		{
			throw new SubscriptionException("No user with User Name "+uname+" exists.");
		}
		
		return ResponseEntity.ok(subs);
	}
	
	
	/**
	 * This method adds a new subscription. If an entry exists in the database
	 * for the user name, the particular entry gets updated,i.e., the subscription
	 * gets updated for the existing user	 * 
	 * @param subs A subscriber object 
	 * @return Added/Updated subscriber object
	 * @throws SubscriptionException 
	 */
	@PostMapping(path = "/newsubs")
	public ResponseEntity<Subscriber> addSubscriber(@RequestBody Subscriber subs) throws SubscriptionException	{	
		Subscriber sub = service.save(subs);	
		return ResponseEntity.ok(sub);
	}
	
	/**
	 * 
	 * @param uname The user name of the subscriber
	 * @return True if the subscription for the user is Active,
	 * 		   False if the subscription for the user is not Active
	 * 		   False if the user does not exist in the table.		
	 * @throws SubscriptionException 
	 */
	
	@GetMapping(path = "/issubscr/{uname}")
	public ResponseEntity<Boolean> isSubscribed(@PathVariable String uname) 
	{
		Boolean isSub = service.isSubscribed(uname);
		return ResponseEntity.ok(isSub);
	}
	
	/**
	 * Find Subscribers subscribed after (Start Date after the date) or 
	 * before (Start Date before the date) a particular date
	 * @param date The reference date (YYYY-MM-DD)
	 * @param afterorbefore true for after and false for before
	 * @return List of subscribers 
	 */
	
	@GetMapping(path = "/subscrba/{date}/{afterorbefore}")
	public  ResponseEntity<List<Subscriber>> getSubscribedAfterOrBefore(@PathVariable String date, 
																	@PathVariable boolean afterorbefore) 
	{
		
		return ResponseEntity.ok(service.getSubscribedAfterOrBefore(date,afterorbefore));
	}
	
	
	/**
	 * The method is used to unsbscribe a user
	 * 
	 * @param uname the user name of the subscriber
	 * @return true if the unsubscription was successful
	 * 		   (User is deletd from the subscription table)	
	 */
	
	//@GetMapping(path = "/unsubscr/{uname}")
	@DeleteMapping(path = "/unsubscr/{uname}")
	public  ResponseEntity<Boolean> deleteSubscription(@PathVariable String uname)
	{
		Boolean delSub = service.deleteSubscription(uname);
		return ResponseEntity.ok(delSub);
	}
	
	
}
