package com.eurowings.task.exception;

public class SubscriptionException extends Exception {

	private static final long serialVersionUID = 8169081502097758066L;
	
	
	 public SubscriptionException(String msg) {
	        super(msg);
	    }
	
	

}
