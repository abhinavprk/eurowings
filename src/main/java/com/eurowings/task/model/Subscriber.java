package com.eurowings.task.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "subscriber")
public class Subscriber implements Serializable {

	private static final long serialVersionUID = 3087113389015586654L;
	
	public Subscriber() {
		}
	
	
	public Subscriber(Long id, @NotNull String userName, @NotNull String firstName, @NotNull String lastName,
			@NotNull @Email String email, @NotNull String startDate, @NotNull String endDate) {
		super();
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.startDate = startDate;
		this.endDate = endDate;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@NotNull
	@Column(name = "uname")
	String userName;
	
	@NotNull
	@Column(name = "fname")
	String firstName;
	
	@NotNull
	@Column(name = "lname")
	String lastName;
	
	@NotNull
	@Email
	@Column(name = "email")
	String email;
	
	@NotNull
	@DateTimeFormat
	@Column(name = "start_date")
	String startDate;
	
	@NotNull
	@DateTimeFormat
	@Column(name = "end_date")
	String endDate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
