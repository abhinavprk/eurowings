package com.eurowings.task.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.eurowings.task.exception.SubscriptionException;
import com.eurowings.task.model.Subscriber;
import com.eurowings.task.repository.SubscriberRepository;

@Service
public class SubscriberServiceImpl implements SubscriberService {

	@Autowired
	SubscriberRepository repo;

	@Override
	public List<Subscriber> getAll() {
		List<Subscriber> subs = new ArrayList<>();
		repo.findAll().forEach(subs::add);
		return subs;
	}

	@Override
	public Subscriber save(Subscriber subs) throws SubscriptionException {
		Subscriber resSub = new Subscriber();

		LocalDate stDate = LocalDate.parse(subs.getStartDate());
		LocalDate endDate = LocalDate.parse(subs.getEndDate());
		if ((stDate.isBefore(LocalDate.now()) || endDate.isBefore(LocalDate.now()) || endDate.isBefore(stDate))) {//Check for the date validity.
			throw new SubscriptionException("Start Date and/or End Date is not valid. Please Check.");
		}

		try {
			Subscriber subsDb = findByUserName(subs.getUserName());
			int res = repo.updateSubscription(subs.getUserName(), subs.getStartDate(), subs.getEndDate());
				if (res == 1) { // if update successful set the new Id to the input object and save it to return object
					subs.setId(subsDb.getId());
					resSub = subs;
				} else { //Else return the not updated object from DB
					resSub = subsDb;
				}		
		} catch (SubscriptionException e) {// if no object in DB save as a new object
			try {
				resSub = repo.save(subs);
			} catch (DataIntegrityViolationException ex) {//In case the email is duplicated
				throw new SubscriptionException(
						"Email already exists in the DB. Please use an unique Email.");
			}
		}
		
		return resSub;
	}

	@Override
	public Subscriber findByUserName(String uname) throws SubscriptionException {

		Subscriber subs = repo.findByUserName(uname);
		if (subs == null) {
			throw new SubscriptionException("Subscriber not found");
		}

		return repo.findByUserName(uname);

	}

	@Override
	public Boolean isSubscribed(String uname) {
		boolean subs = false;
		try {
			Subscriber subscriber = findByUserName(uname);
			Subscriber subObj = subscriber;
			LocalDate startDate = LocalDate.parse(subObj.getStartDate());
			LocalDate endDate = LocalDate.parse(subObj.getEndDate());
			LocalDate currDate = LocalDate.now();
			if ((startDate.isEqual(currDate) || startDate.isBefore(currDate))
						&& (endDate.isEqual(currDate) || endDate.isAfter(currDate))) {
					subs = true;
			}

		} catch (SubscriptionException e) {
			e.printStackTrace();
		}

		return subs;
	}

	@Override
	public List<Subscriber> getAllSubs() {		
		List<Subscriber> subs = new ArrayList<>();
		String sDate = LocalDate.now().plus(Period.ofDays(1)).toString();
		String eDate = LocalDate.now().minus(Period.ofDays(1)).toString();
		repo.findByStartDateBeforeAndEndDateAfter(sDate,eDate).forEach(subs::add);
		return subs;
	}

	@Override
	public List<Subscriber> getAllUnSubs() {
		List<Subscriber> unSubs = new ArrayList<>();
		String date = LocalDate.now().toString();
		repo.findByStartDateAfterOrEndDateBefore(date,date).forEach(unSubs::add);
		return unSubs;
	}

	@Override
	public Boolean deleteSubscription(String uname) {

		boolean result = true;

		try {
			repo.deleteByUserName(uname);
			
		} catch (IllegalArgumentException  e) {
			result = false;
		}

		return result;

	}

	@Override
	public List<Subscriber> getSubscribedAfterOrBefore(String date, boolean afterorbefore) {
		List<Subscriber> subsc = new ArrayList<>();
		if(afterorbefore)
		{
			repo.findByStartDateAfter(date.toString()).forEach(subsc::add);
		}else
		{
			repo.findByStartDateBefore(date.toString()).forEach(subsc::add);
		}
		return subsc;
	}

}
