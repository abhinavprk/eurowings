package com.eurowings.task.service;

import java.util.List;

import com.eurowings.task.exception.SubscriptionException;
import com.eurowings.task.model.Subscriber;

public interface SubscriberService {
	
	/**
	 * 
	 * @return List of Subscribers from database
	 */
	public List<Subscriber> getAll();
	  
	/**
	 * 
	 * @param subs Subscriber Object to be saved
	 * @return Saved object or the SubscriptionException stating the reason of save failure
	 * @throws SubscriptionException
	 */
	public Subscriber save(Subscriber subs) throws SubscriptionException;

	/**
	 * 
	 * @param uname userName of the Subscriber
	 * @return Subscriber Object if exists or Subscription Exception stating that user not found
	 * @throws SubscriptionException
	 */
	public Subscriber findByUserName(String uname) throws SubscriptionException;

	/**
	 * 
	 * @param uname userName of the Subscriber
	 * @return true if the user is currently Subscribed
	 * 		   false if the user is not subscribed
	 * 		   false if user does not exist in the database		
	 */
	public Boolean isSubscribed(String uname);

	/**
	 * 
	 * @return A list of all Subscribers currently subscribed to the 
	 * 		   newsletter	
	 */
	public List<Subscriber> getAllSubs();

	/**
	 * 
	 * @return A List of Subscribers currently not subscribed to the newsletter
	 */
	public List<Subscriber> getAllUnSubs();

	/**
	 * 
	 * @param uname userName of the Subscriber
	 * @return true if the deletion is successful 
	 * 	       fasle if the deletion is not successful	
	 */
	public Boolean deleteSubscription(String uname);

	/**
	 * 
	 * @param date the reference date in YYYY-MM-DD format
	 * @param afterorbefore true for the list of Subscribers subscribed after the reference date
	 * 						false for the list of Subscribers subscribed before the reference date		
	 * @return A list of Subscribers
	 */
	public  List<Subscriber> getSubscribedAfterOrBefore(String date, boolean afterorbefore);

}
