package com.eurowings.task.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.eurowings.task.model.Subscriber;

@RestResource(exported=false)
public interface SubscriberRepository extends PagingAndSortingRepository<Subscriber, Long> {
	
	Subscriber findByUserName(String uname);
	
	Iterable<Subscriber> findByStartDateBeforeAndEndDateAfter(String sDate, String eDate);
	

	Iterable<Subscriber> findByStartDateAfterOrEndDateBefore(String sDate, String eDate);
	
	
	@Modifying
	@Query(value = "UPDATE Subscriber s SET s.startDate = :sDate, s.endDate = :eDate WHERE s.userName = :uname")
	@Transactional
	int updateSubscription( @Param("uname")String uname, @Param("sDate") String startDate, @Param("eDate") String endDate);

	@Modifying
	@Query(value = "DELETE FROM Subscriber s WHERE s.userName = :uname")
	@Transactional
	void deleteByUserName(@Param("uname") String uname);
	
	Iterable<Subscriber> findByStartDateAfter(String date);
	
	Iterable<Subscriber> findByStartDateBefore(String date);
}

