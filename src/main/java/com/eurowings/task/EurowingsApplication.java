package com.eurowings.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurowingsApplication {

	public static void main(String[] args) {
		System.out.println("Starting Eurowings!");
		SpringApplication.run(EurowingsApplication.class, args);
		System.out.println("Eurowings Started!");
	}
}
