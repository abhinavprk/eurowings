##Eurowings Subscriber REST API

---
**The API has been coded only to fulfill the requirements mentioned in the challenge. There are minimal improvisations on the top of the requirements**

---

**Functions of API**

This API serves the following purposes:

1.	A Subscriber can be created with the subscription to the newsletter.
2.	A List of all the Subscriber users can be accessed using the API.
3.	A List of all Subscriber users currently subscribed to the newsletter can be retrieved.
4.	A List of all Subscribers users currently not subscribed to the newsletter can be retrieved.
5.  API can be queried to find out subscription details of a particular Subscription user.
6.	API can be queried to find out if a Subscriber user is currently subscribed to the newsletter or 	not.
7.	A list of Subscriber users subscribed to the newsletter after of before a particular date can be 
	retrieved.
8.  A Subscriber user can be removed from the database and hence the subscription of newsletter for 	that particular user. 

---

** Technology Stack **

1.	Java 1.8
2.	Spring 5.0.9
3. 	Spring Boot 2.0.5
4.	Liquibase 3.5.5
5.	MySQLDB 5.1.47
6. 	H2 DB 1.49.197
7. 	JUnit 4.12

H2 DB has been used primarily to provide in memory database. 

Application has been tested using MySQL DB as well. 

Configuration can be found in _src/main/resources/application.properties_ file.

---

** Database Related Info **

Liquibase has been used to create the database schema on runtime and populate the initial values
in the database. 

The related configuration files are:

1. _src/main/resources/db/changelog/data.csv_  : File with initial data to be populated in the table
2. _src/main/resources/db/changelog/db.changelog-master.xml_ : Master changelog file
3. _src/main/resources/db/changelog/db.changelog-1.0.xml_ : changelog file with table create and inseart info.

Schema Name: _eurowings_

Table Name: _subscriber_

There is only one table needed based on the requirements. The Table description is as follows.

|**Column Name**  | **Type**         | **Unique?** | **Null allowed?**  | **Primary Key**|

|     id      | BIGINT       |   Yes   |      No        |     Yes    |

|    uname    | VARCHAR(255) |   Yes   |      No        |      No    |

|    email    | VARCHAR(255) |   Yes   |      No        |      No    |

|    fname    | VARCHAR(255) |   No    |      Yes       |      No    |

|    lname    | VARCHAR(255) |   No    |      Yes       |      No    |

|  start_date | DATE         |   No    |      No        |      No    |

|   end_date  | DATE         |   No    |      No        |      No    |


Initially 6 entries are inserted in the table at application startup with the following user names:

aprakash

anshu

navod

jmax

nprom

riya05

  

---

** Table and DAO class mapping **

Subscriber.java is the DAO class for subscriber table. It has following properties (The respective columns from subscriber table are on the right side of the column (:)):

Long id : id

String userName : uname

String firstName : fname

String lastName : lname

String email : email      (Must be in valid email format)

String startDate : start_date  (Must be in YYYY-MM-DD format)

String endDate : end_date	(Must be in YYYY-MM-DD format)

---



** API End Points **

_SubscriberController.java_ is the Rest controller class. It has following end points:

 _/api/all_	
		
		:Request Type: GET
		:Call to this end point returns all the entries from the subscriber db in JSON format.
		:This end point does not need any input parameters.
		:Example call: http://localhost:8080/api/all
		
 _/api/allsubs_
		
		:Request Type: GET
		:Call to this end point returns all currently subscribed Subscribers from the subscriber db in JSON format.
		:This end point does not need any input parameters.
		:Example call: http://localhost:8080/api/allsubs
		
 _/api/allunsubs_
		
		:Request Type: GET
		:Call to this end point returns all Subscribers currently not subscribed from the subscriber db in JSON format.
		:This end point does not need any input parameters.
		:Example call: http://localhost:8080/api/allunsubs
		
 _/api/subs/{uname}_

		:Request Type: GET
		:Call to this end point returns the Subscriber with userName = {uname} from the subscriber db in JSON format.
		:This end point needs the user name of the Subscriber as a parameter.
		:Example call: http://localhost:8080/api/subs/aprakash
		
		
 _/api/newsubs_
		
		:Request Type: POST
		:Call to this end point adds/updates a Subscriber and subscribes to the newsletter afterwards returns the added/updated Subscriber from the subscriber db in JSON format.
		:This end point needs a Subscriber Object in JSON format as a parameter in the request Body.
		:Example call: http://localhost:8080/api/newsubs
		:Example input: 
						{
						"userName": "anas",
						"firstName": "Kiki",
						"lastName": "John",
						"email": "email@kiki.com",
						"startDate": "2018-12-13",
						"endDate": "2019-05-13"
						}
						
		Things to take care : userName should be unique for a new Subscriber
							:email should be unique for a new Subscriber. The functionality to update the email of existing Subscriber is not present.
							:startDate must be before endDate
							:startDate and endDate must be On or after current date
		
_/api/issubscr/{uname}_

		:Request Type: GET
		:Call to this end point returns true if the Subscriber with user name {uname} is currently subscribed to the newsletter otherwise false
		:This end point needs the user name of the Subscriber as a parameter.
		:Example call: http://localhost:8080/api/issubscr/aprakash					


_/api/subscrba/{date}/{afterorbefore}_
		
		:Request Type: GET
		:Call to this end point returns List of all Subscribers subscribed after/before a reference date in JASON format
		:This end point needs a date in YYYY-MM-DD format and true (for after) or false (for before) 
		:Example call: http://localhost:8080/api/subscrba/2018-06-01/true
					   -->Returns all Subscribers who subscribed after 1st June 2018.		
					   :http://localhost:8080/api/subscrba/2018-06-01/false
					   -->Returns all Subscribers who subscribed before 1st June 2018.
					   
					   
_/api/unsubscr/{uname}_
		
		:Request Type: GET
		:Call to this end point deletes the subscription for a particular Subscriber and returns true or false based on the success of the deletion operation
		:This end point needs the user name of the Subscriber as a parameter.
		:Example call: http://localhost:8080/api/unsubscr/aprakash					   
					   
					   
---

** Running,and Accessing the API **

Running:
		
		:Download the eurowings/jar/eurowings.jar file to your machine. 
		:Open console window and move to the location of the jar file. For example in Windows: (cd C:\Users\Abhinav\Documents\repos\eurowings\jar)
		:Run the following command: java -jar eurowings.jar
		:Wait until the api starts: A message "Eurowings Started!" on the console asserts that teh api is up and running and can be accessed.
		
		
Accessing the API:

		: Use a tool like Postman (https://www.getpostman.com/apps) to access the API through the end points explained above. A browser can also be used for this purpose but Postman like tools would be easier.
		:The app by default uses H2 in-memeory database by default. No need to change database settings if you don't really want to work with other database. MySQL dependencies are mentioned in pom.xml and configurations are in the application.properties file 


---

** Building the API **

1. Clone the api from  https://abhinavprk@bitbucket.org/abhinavprk/eurowings.git
2. Import the project in Eclipse 
3. Change Database Config in application.properties, only if needed.
4. To generate jar file, Run As --> maven install
5. To run from Eclipse, Run As --> Spring Boot App

Note: IDE must have Spring tools, JUnit Plugin, and  Java 1.8 dependencies installed.

---

** Unit Test **

1. To run Unit Test, JUnit 4 has been used.
2. EclEmma (https://www.eclemma.org/) has been used to check unit test coverage. 

---

** Docker Settings **

For Windows:
		
		:To build docker image, run the following file from the root of the project
			_buildDockerWin.bat_  
		:To run docker image, run the following file 
			_runDockerWin.bat_ 
		:API can be accessed on _http://localhost:8080/_
		
		
For Linux:
		
		:To build docker image, run the following Script from the root of the project
			_buildDockerLinux.sh_  
		:To run docker image, run the following file 
			_runDockerLinux.sh_ 
		:API can be accessed on _http://localhost:8080/_
		

Docker image name = eurowings-docker

---
